#include <ioCC2541.h>     
#include "ledapp.h"

void Led_Init(void)
{
 //设置P0,P1端口方向为输出(作用控制LED灯的显示)
  P0DIR|=0xff;
  P1DIR|=0xff;
  P0SEL &= ~0xff;
  P1SEL &= ~0xff;
}