#include <ioCC2541.h>     
#include "keyapp.h"    
    
/*********************宏定义************************/    
//注册时使用的宏    
#define NO_TASK_ID                      0xFF            //没有注册时的任务id    
#define NO_EVEN_ID                      0x0000          //没有注册时的事件id    
    
//中断消抖时使用的宏    
#define KEY_DEBOUNCE_VALUE  1          //消抖时间1ms    
     
/*********************内部变量************************/    
static uint8 registeredKeyTaskID = NO_TASK_ID;    
static uint16 registeredKeyEvenID = NO_EVEN_ID;    
    
    
/*********************函数声明************************/    
extern uint8 osal_start_timerEx( uint8 task_id, uint16 event_id, uint32 timeout_value );  

//按键初始化
void Key_Init(void)    
{      
  P2SEL &= ~0x01;      //P2.0设置为通用I/O口
  P2DIR &= ~0x01;      //P2.0设置为输入
  P2IFG &= ~0x01;      //P2.0中断状态标志位清0
  P2INP &= ~BIT7;      //设置P2口全部上拉    
  PICTL |=  0x08;      //设置P2口下降沿触发     
  P2IEN |=  0x01;      //使能P2.0中断      
  IEN2  |=  0x02;          //允许P1口中断;     
}


//注册按键
uint8 RegisterForKey(uint8 task_id, uint16 even_id)    
{
  // Allow only the first task    
  if ( registeredKeyTaskID == NO_TASK_ID )    
  {    
    registeredKeyTaskID = task_id;
    //return ( TRUE );
  }    
  else    
    return ( FALSE );    

  // Allow only the first even    
  if ( registeredKeyEvenID == NO_EVEN_ID )    
  {
    registeredKeyEvenID = even_id;    
  } 
  else    
    return ( FALSE );    
 
  return ( TRUE );      
}


//检测按键高低电平
uint8 Key_Check_Pin(void)    
{    
  if((uint8)0x01 & P2==0)    
  {    
    return KEY_LOOSEN;
  }    
  else    
  {    
    return KEY_PRESS;     
  }    
}

//P2口按键中断函数

//#pragma vector = P2INT_VECTOR        
//__interrupt void P2_ISR(void)     
//{   
//    
//    if(Key_Check_Pin() == KEY_PRESS)     
//    {
//      //延时消抖
//      osal_start_timerEx(registeredKeyTaskID, registeredKeyEvenID, KEY_DEBOUNCE_VALUE);
//    }    
//    P2IFG = 0;       //清中断标志     
//    P2IF = 0;        //清中断标志 
//}