#ifndef KEYAPP_H  
#define KEYAPP_H  

#include "hal_types.h"
#define  BIT0             (1 << 0)
#define  BIT1             (1 << 1)
#define  BIT2             (1 << 2)
#define  BIT3             (1 << 3)
#define  BIT4             (1 << 4)
#define  BIT5             (1 << 5)
#define  BIT6             (1 << 6)
#define  BIT7             (1 << 7)

#ifndef High    
#define  High              1   
#define  Low               0
#endif

 
//检测io口状态时使用的宏  
#define KEY_LOOSEN                      0x01              
#define KEY_PRESS                       0x00  
#define SBP_KEY_CHECK_PROCESS_EVT       0x1000  //按键检测处理事件 
  
/*********************函数声明************************/  
extern void Key_Init(void);  
extern uint8 RegisterForKey(uint8 task_id, uint16 even_id);  
extern uint8 Key_Check_Pin(void);  
  
#endif  